# CYGWIN


## Setup

Install Cygwin by running the (setup-x86_64.exe)[https://cygwin.com/setup-x86_64.exe]    
(Cygwin DLL 2.8.2, 2017-08-27) 


## Package managers

Cygwin (and Babun) use custom written package managers (Babun has `pact` preinstalled).
Some popular choices are:
- `sage`

- `apt-cyg` (unmaintained)



(cygwin)[https://cygwin.com]
(sage)[https://github.com/svnpenn/sage]
(apt-cyg)[https://github.com/transcode-open/apt-cyg]
(babun)[https://github.com/babun/babun]


## Mirrors
https://cygwin.com/mirrors.html
