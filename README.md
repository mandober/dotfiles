# POSIX dotfiles

DESC: Dotfiles for POSIX environments
REPO: https://bitbucket.org/mandober/dotfiles
git remote add backup git@github.com:mandober/dotfiles.git

LANG: bash
DATE: 2017-08-27


## DESCRIPTION

Shell scripts (bash) and dotfiles for miscellaneous POSIX environments:    
- Linux
- WSL (Windows Subsystem for Linux a.k.a Bash on Windows)
- Cygwin   
- MSYS2

*Note about package managers*
- `apt`: Ubuntu (LinuxMint) and WSL
- `pacman`: ArchLinux and MSYS2
- Cygwin (and Babun) use custom written package managers (Babun has `pact` preinstalled);    
  `apt-cyg` and `sage` are popular package managers for Cygwin (latter is still maintained, former is not)


(cygwin)[https://cygwin.com]
(sage)[https://github.com/svnpenn/sage]
(apt-cyg)[https://github.com/transcode-open/apt-cyg]
(babun)[https://github.com/babun/babun]



## SETUP

*`MSYSTEM`*
Set variable `MSYSTEM` to appropriate environment   
(no need to set it in MSYS2 since it already exists there).    

For example:    
```bash
export MSYSTEM=LINUX
export MSYSTEM=WSL
export MSYSTEM=CYGWIN
export MSYSTEM=MINGW64
export MSYSTEM=MSYS
export MSYSTEM=GITBASH
```

*`DOTS`*


